package com.example.gabrielguevara.expressions;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;

/**
 * Created by gabrielguevara on 5/4/17.
 */

public class JEXL {

    public static String excuteExpression () {
        // Create or retrieve an engine
        JexlEngine jexl = new JexlBuilder().create();

        // Create an expression
        String jexlExp = "(3 + 10) + 5 * ((- 29 * 3 + 8 + 100) + 3) > 100";
        JexlExpression e = jexl.createExpression(jexlExp);

        // Create a context and add data
        JexlContext jc = new MapContext();
        String text = null;
        try {

            //jc.set("foo", new Foo() );

            // Now evaluate the expression, getting the result
            int o = (int) e.evaluate(jc);
            text = ( String.valueOf(o) );
        } catch (Exception ex) {
            try {

                //jc.set("foo", new Foo() );

                // Now evaluate the expression, getting the result
                boolean o = (boolean) e.evaluate(jc);
                text = ( String.valueOf(o) );
            } catch (Exception ex2) {
                try {

                    //jc.set("foo", new Foo() );

                    // Now evaluate the expression, getting the result
                    double o = (double) e.evaluate(jc);
                    text = ( String.valueOf(o) );
                } catch (Exception ex3) {
                    ex3.printStackTrace();

                }
            }
        }

        return text;
    }

}
