package com.example.gabrielguevara.expressions;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.JxltEngine;
import org.apache.commons.jexl3.MapContext;

public class MainActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = (Button) findViewById(R.id.buttonA);
        text = (TextView) findViewById(R.id.myText);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //JEXL
                text.setText( JEXL.excuteExpression() );


            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();


    }
}
