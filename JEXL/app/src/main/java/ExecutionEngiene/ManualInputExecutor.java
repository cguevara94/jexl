package ExecutionEngiene;

import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlException;
import org.apache.commons.jexl3.JexlExpression;

import java.util.Arrays;
import java.util.List;

/**
 * Created by gabrielguevara on 25/4/17.
 */

public class ManualInputExecutor extends Executor {

    List<String> variablesToInsert = null;

    public ManualInputExecutor(DiagramShape shape, String processName) {
        super(shape, processName);
        notAllowedCharacters = new String[0];
    }

    @Override
    public void executeStep() throws JexlException, ClassCastException {
        String text = shapeToExecute.textShape.string;
        text = text.trim();
        this.variablesToInsert = Arrays.asList(text.split( "," ));
        insertNextVariable();
    }

    private void insertNextVariable() {
        String variable = "";
        if ( variablesToInsert != null && variablesToInsert.size() > 0 ) {
            variable = variablesToInsert.get(0);
            variable = variable.trim();
            AlertsManager alertManager = new AlertsManager();
            alertManager.showInputAlert(ExecutionContext.getSharedInstance().currentActivity, variable, this);
        } else {
            didFinishInsertions();
        }
    }

    private void didFinishInsertions() {
        if ( !ExecutionContext.getSharedInstance().isExecutionByStep ) {
            nextStep();
        }
    }

    public void continueManualInput(String text) {
        String firstVariable = variablesToInsert.remove(0);

        JexlEngine engine = ExecutionContext.getSharedInstance().jexl;
        JexlExpression expression = engine.createExpression("var "+firstVariable+" = "+text+"");
        expression.evaluate(context);

        if ( variablesToInsert != null && variablesToInsert.size() > 0 ) {
            insertNextVariable();
        } else {
            didFinishInsertions();
        }
    }
}
