package ExecutionEngiene;

import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlException;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;

import java.util.ArrayList;

/**
 * Created by gabrielguevara on 25/4/17.
 */

public class ProcessExecutor extends Executor {

    public ProcessExecutor(DiagramShape shape, String processName) {
        super(shape, processName);
        notAllowedCharacters = new String[0];
    }

    @Override
    public void executeStep() throws JexlException, ClassCastException {
        String text = shapeToExecute.textShape.string;

        JexlEngine engine = ExecutionContext.getSharedInstance().jexl;
        JexlExpression expression = engine.createExpression(text);
        expression.evaluate(context);

        if (!ExecutionContext.getSharedInstance().isExecutionByStep) {
            nextStep();
        }
    }
}
