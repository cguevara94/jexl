package ExecutionEngiene;

import android.app.Activity;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlException;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by gabrielguevara on 24/4/17.
 */

enum ErrorType {

    ValidationError, ExecutionError

}

public class ExecutionContext {

    JexlEngine jexl = new JexlBuilder().create();
    private static ExecutionContext sharedInstance;
    Map<String, JexlContext> context;
    String currentProcess;
    Activity currentActivity;
    Boolean isExecutionByStep;
    Map<String, List<DiagramShape>> terminators;
    Executor currentStep;
    Boolean isErrorOcurred;
    ErrorType errorType;

    private ExecutionContext() {
        terminators.put(currentProcess, new ArrayList<DiagramShape>());
    }

    public static synchronized ExecutionContext getSharedInstance(){
        if (sharedInstance == null) {
            sharedInstance = new ExecutionContext();
        }
        return sharedInstance;
    }

    public JexlContext retrieveContextByProcessName(String processName) {

        JexlContext contextToRetrieve = context.get(processName);
        if (contextToRetrieve == null) {
            context.put(processName, new MapContext());
                    contextToRetrieve = context.get(processName);
            //addExceptionHandler(contextToRetrieve);
        }
        return contextToRetrieve;

    }

    public void addTerminator(DiagramShape terminator) {
        terminators.get(currentProcess).add(terminator);
    }

    public void run() {
        isErrorOcurred = false;
        List<DiagramShape> terminatorsInProcess = terminators.get(currentProcess);
        DiagramShape startShape;
        for (DiagramShape terminator : terminatorsInProcess) {
            ConnectionLine[] connectionLines = new ConnectionLine[]{terminator.connectorDown, terminator.connectorLeft, terminator.connectorRight, terminator.connectorUp};
            List<ConnectionLine> conectors = Arrays.asList(connectionLines);
            for (ConnectionLine connector : conectors) {
                if (connector != null && connector.isOriginShape(terminator)) {
                    startShape = connector.originConnector;
                    startShape.executor.execute();
                    break;
                }
            }
        }
    }

    public void addExceptionHandler(JexlContext context) {

    }

    public void  setExecutionWithError(ErrorType errorType, String errorMessage, Boolean showAlert) {
        if (this.currentStep != null) {
            this.isErrorOcurred = true;
            this.currentStep.shapeToExecute.optionShape.showLikeError();
            this.errorType = errorType;
            if (showAlert != null && showAlert) {
                    showErrorAlert(errorMessage);
            }
        }
    }

    public void showErrorAlert(String error) {
        AlertsManager alertM = new AlertsManager();
        alertM.showErrorAlert(this.currentActivity, error);
    }

}
