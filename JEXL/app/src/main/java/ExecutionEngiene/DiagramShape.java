package ExecutionEngiene;

import android.graphics.Point;
/**
 * Created by gabrielguevara on 24/4/17.
 */

class Shape{}
class TextShape { String string; }
class OptionShape {

    public void showLikeError(){}

}

public class DiagramShape {

    Shape shape;
    TextShape textShape;
    OptionShape optionShape;
    ConnectionLine connectorUp;
    ConnectionLine connectorDown;
    ConnectionLine connectorRight;
    ConnectionLine connectorLeft;
    Executable executor;
    public String tipo;

    public void create(String tipo) {


    }

    public void loadDataSVG(String tipo){}

    public void drawShapeSVG(){}

    public Point retrieveConnectionPoint(ConnectionLine line) {return null;}

    public void disconnectLine(ConnectionLine line) {}

    public ConnectionLine[] disconnectAllLines() {return null;}

    public String retrieveConnectionLabel(ConnectionLine line) {return null;}

    public void move(Point location){}

}
