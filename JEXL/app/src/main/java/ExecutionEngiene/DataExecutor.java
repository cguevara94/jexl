package ExecutionEngiene;

import org.apache.commons.jexl3.JexlException;

/**
 * Created by gabrielguevara on 26/4/17.
 */

public class DataExecutor extends Executor {

    public DataExecutor(DiagramShape shape, String processName) {
        super(shape, processName);
        notAllowedCharacters = new String[0];
    }

    @Override
    public void executeStep() throws JexlException, ClassCastException {
        if ( !ExecutionContext.getSharedInstance().isExecutionByStep ) {
            nextStep();
        }
    }
}
