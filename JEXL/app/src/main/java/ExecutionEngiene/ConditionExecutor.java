package ExecutionEngiene;

import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlException;
import org.apache.commons.jexl3.JexlExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gabrielguevara on 26/4/17.
 */

public class ConditionExecutor extends Executor {

    Boolean conditionResult;

    public ConditionExecutor(DiagramShape shape, String processName) {
        super(shape, processName);
        notAllowedCharacters = new String[0];
    }

    @Override
    public void executeStep() throws JexlException, ClassCastException {
        String text = shapeToExecute.textShape.string;
        String textInCondition = text;

        JexlEngine engine = ExecutionContext.getSharedInstance().jexl;
        JexlExpression expression = engine.createExpression(text);
        conditionResult = (boolean) expression.evaluate(context);

        if ( !ExecutionContext.getSharedInstance().isExecutionByStep ) {
            nextStep();
        }
    }

    @Override
    public void nextStep() {
        List<ConnectionLine> conectors = new ArrayList<>();
        conectors.add(shapeToExecute.connectorDown);
        conectors.add(shapeToExecute.connectorLeft);
        conectors.add(shapeToExecute.connectorRight);
        conectors.add(shapeToExecute.connectorUp);
        for ( ConnectionLine connector : conectors ) {
            if ( connector != null && connector.isOriginShape(shapeToExecute) ) {
                String routeLabel = connector.connectorDescription.string;
                if ( ( routeLabel == "Si" && conditionResult == true ) || ( routeLabel == "No" && conditionResult == false ) ) {
                    connector.destinyConnector.executor.execute();
                }
            }
        }
    }
}
