package ExecutionEngiene;

/**
 * Created by gabrielguevara on 26/4/17.
 */

interface  ExecutorType {
    String TERMINATOR = "Terminator", DATA = "Data", DISPLAY = "Display", DECISION = "Desicion", PROCESS = "Process", PREPARATION = "Preparation", MANUAL_INPUT = "ManualInput";

}

public class ExecutorFactory {

    public static Executable create(DiagramShape shape, String processName) {

        switch (shape.tipo) {
        case ExecutorType.TERMINATOR:
            return new TerminatorExecutor(shape, processName);

        case ExecutorType.DATA:
            return new DataExecutor(shape, processName);

        case ExecutorType.DISPLAY:
            return new DisplayExecutor(shape, processName);

        case ExecutorType.DECISION:
            return new ConditionExecutor(shape, processName);

        case ExecutorType.PROCESS:
            return new ProcessExecutor(shape, processName);

        case ExecutorType.MANUAL_INPUT:
            return new ManualInputExecutor(shape, processName);

        case ExecutorType.PREPARATION:
            return new PreparationExecutor(shape, processName);

        default:
            return null;
        }
    }

}
