package ExecutionEngiene;

import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlException;
import org.apache.commons.jexl3.JexlExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gabrielguevara on 26/4/17.
 */

public class PreparationExecutor extends Executor {

    Boolean isActiveBucle = false;
    String condition;
    String counter;
    String increment;

    public PreparationExecutor(DiagramShape shape, String processName) {
        super(shape, processName);
        notAllowedCharacters = new String[0];
    }

    @Override
    public void executeStep() throws JexlException, ClassCastException {
        JexlEngine engine = ExecutionContext.getSharedInstance().jexl;
        if ( !isActiveBucle ) {
            String text = shapeToExecute.textShape.string;
            asignarVariables( text );
            isActiveBucle = true;
            JexlExpression expression = engine.createExpression(counter);
            expression.evaluate(context);

            setLastBlockOfBucle();
        } else {
            JexlExpression expression = engine.createExpression(increment);
            expression.evaluate(context);
        }
        JexlExpression expression = engine.createExpression(condition);
        Boolean conditionResult = (Boolean) expression.evaluate(context);

        if (conditionResult) {
            if ( !ExecutionContext.getSharedInstance().isExecutionByStep ) {
                nextStep();
            }
        } else {
            if ( !ExecutionContext.getSharedInstance().isExecutionByStep ) {
                isActiveBucle = false;
                nextStepOutOfBucle();
            }
        }
    }

    private void asignarVariables(String text) {
        String[] separatedVariables = text.split(";");
        counter = separatedVariables[0].trim();
        condition = separatedVariables[1].trim();
        increment = separatedVariables[2].trim();
    }

    @Override
    public void nextStep() {
        nextStepInSecuence();
    }

    public void setLastBlockOfBucle() {
        List<ConnectionLine> conectors = new ArrayList<>();
        conectors.add(shapeToExecute.connectorLeft);
        conectors.add(shapeToExecute.connectorRight);

        for ( ConnectionLine connector : conectors ) {
            if ( connector != null && !connector.isOriginShape(shapeToExecute) ) {
                connector.originConnector.executor.isLastBucleStep(true);
            }
        }
    }
}
