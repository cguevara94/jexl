package ExecutionEngiene;

import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlException;
import org.apache.commons.jexl3.MapContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gabrielguevara on 24/4/17.
 */

public abstract class Executor implements Executable {

    JexlContext context;
    String sentenceToExecute;
    String result;
    String[] notAllowedCharacters;
    Boolean isLastBucleStep;
    DiagramShape shapeToExecute;

    public Executor(DiagramShape shape, String processName) {
        context = ExecutionContext.getSharedInstance().retrieveContextByProcessName(processName);
        this.shapeToExecute = shape;
        isLastBucleStep = false;
    }

    @Override
    public void execute() {
        try {
            if (!ExecutionContext.getSharedInstance().isErrorOcurred && validate()) {
                ExecutionContext.getSharedInstance().currentStep = this;
                executeStep();
            } else {
                setValidationErrors();
            }
        } catch (JexlException | ClassCastException exception) {
            String error = exception.getMessage() != null ? exception.getMessage() : "unknown error";
            ExecutionContext.getSharedInstance().setExecutionWithError(ErrorType.ExecutionError, error, true);
        }
    }

    private void setValidationErrors() {
        ExecutionContext.getSharedInstance()
                .setExecutionWithError(ErrorType.ValidationError, "Error de validación, compruebe la sintaxis", true);
    }

    @Override
    public abstract void executeStep() throws JexlException, ClassCastException;

    @Override
    public Boolean validate() {
        return true;
    }

    @Override
    public void nextStep() {
        if (isLastBucleStep) {
            nextBucleIteration();
        } else {
            nextStepInSecuence();
        }
    }

    @Override
    public void nextBucleIteration() {
        List<ConnectionLine> bucleConectors = new ArrayList<>();
        bucleConectors.add(shapeToExecute.connectorLeft);
        bucleConectors.add(shapeToExecute.connectorRight);
        for ( ConnectionLine connector : bucleConectors ) {
            if ( connector != null && connector.isOriginShape(shapeToExecute) ) {
                connector.destinyConnector.executor.execute();
                return;
            }
        }
    }

    @Override
    public void nextStepInSecuence() {
        List<ConnectionLine> connectors = new ArrayList<>();
        connectors.add(shapeToExecute.connectorDown);
        connectors.add(shapeToExecute.connectorUp);

        for (ConnectionLine connector : connectors) {
            if ( connector != null && connector.isOriginShape(shapeToExecute) ) {
                connector.destinyConnector.executor.execute();
            }
        }
    }

    public void nextStepOutOfBucle() {
        List<ConnectionLine>  conectors = new ArrayList<>();
        conectors.add(shapeToExecute.connectorLeft);
        conectors.add(shapeToExecute.connectorRight);
        for ( ConnectionLine connector : conectors) {
            if ( connector != null && !connector.isOriginShape(shapeToExecute) ) {
                connector.originConnector.executor.nextStepInSecuence();
            }
        }
    }

    @Override
    public JexlContext swapContext(JexlContext context) {
        JexlContext auxContext = this.context;
        this.context = context;
        return auxContext;
    }

    @Override
    public void isLastBucleStep(Boolean isLastBucleStep) {
        this.isLastBucleStep = isLastBucleStep;
    }
}
