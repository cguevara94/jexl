package ExecutionEngiene;

import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlException;
import org.apache.commons.jexl3.JexlExpression;

/**
 * Created by gabrielguevara on 25/4/17.
 */

public class DisplayExecutor extends Executor {

    public DisplayExecutor(DiagramShape shape, String processName) {
        super(shape, processName);
        notAllowedCharacters = new String[0];
    }

    @Override
    public void executeStep() throws JexlException, ClassCastException {
        String text = shapeToExecute.textShape.string;

        JexlEngine engine = ExecutionContext.getSharedInstance().jexl;
        JexlExpression expression = engine.createExpression(text);

        String textToDisplay = (String) expression.evaluate(context);

        AlertsManager alertManager = new AlertsManager();
        alertManager.showDisplayAlert( ExecutionContext.getSharedInstance().currentActivity, textToDisplay, this);
    }

    public void continueDisplay() {
        if ( !ExecutionContext.getSharedInstance().isExecutionByStep ) {
            nextStep();
        }
    }

}
