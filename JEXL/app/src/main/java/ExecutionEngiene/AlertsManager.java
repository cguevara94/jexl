package ExecutionEngiene;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.widget.EditText;

/**
 * Created by gabrielguevara on 25/4/17.
 */

public class AlertsManager {

    public void showInputAlert(Activity activity, String inputVariable, final ManualInputExecutor manualInputEx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Input");
        builder.setMessage("Ingrese un valor para '"+inputVariable+"'");

        // Set up the input
        final EditText input = new EditText(activity);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        input.setHint("Ingrese un valor");
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String text = input.getText().toString();
                manualInputEx.continueManualInput(text);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void showDisplayAlert(Activity activity, String message, final DisplayExecutor displayExecutor) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Display");
        builder.setMessage(message);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                displayExecutor.continueDisplay();
                dialog.dismiss();
            }
        });

        builder.show();

    }

    public void showErrorAlert(Activity activity, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Error");
        builder.setMessage(message);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

}
