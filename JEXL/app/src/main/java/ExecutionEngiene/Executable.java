package ExecutionEngiene;

import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlException;

/**
 * Created by gabrielguevara on 24/4/17.
 */

public interface Executable {

    void execute();
    void executeStep() throws JexlException, ClassCastException;
    Boolean validate();
    void nextStep();
    void nextBucleIteration();
    void nextStepInSecuence();
    JexlContext swapContext(JexlContext context);
    void isLastBucleStep(Boolean isLastBucleStep);

}
