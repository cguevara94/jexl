package com.jexl.gabrielguevara.jexl;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;

public class MainActivity extends AppCompatActivity {

    private static final JexlEngine jexl = new JexlBuilder().cache(512).strict(true).silent(false).create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assuming we have a JexlEngine instance initialized in our class named 'jexl':
        // Create an expression object for our calculation
        String calculateTax = "((10 + 2 + 32) * 0.1) + 9"; //e.g. "((G1 + G2 + G3) * 0.1) + G4";
        String calculateTax1 = "a = 3"; //e.g. "((G1 + G2 + G3) * 0.1) + G4";
        String calculateTax2 = "c = a + 2"; //e.g. "((G1 + G2 + G3) * 0.1) + G4";
        JexlExpression e = jexl.createExpression( calculateTax );

        // populate the context
        JexlContext context = new MapContext();
        context.set("G1", "12");
        context.set("G2", "2");
        context.set("G3", "4");
        context.set("G4", "-1");
        // ...

        // work it out
        Number result = (Number) e.evaluate(context);

        Button button = (Button) findViewById(R.id.executeButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView text = (TextView) findViewById(R.id.textResult);
                JEXL.excuteExpression("a = 3");
                JEXL.excuteExpression("b = 5");
                JEXL.excuteExpression("c = a + b");
                text.setText( JEXL.excuteExpression("c") );
            }
        });
    }
}
